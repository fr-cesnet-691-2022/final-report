#!/bin/bash
ansible-playbook -i hosts config-server.yml --extra-vars @vars/vault-vars.yml --vault-password-file=ansible-vault-passwords
