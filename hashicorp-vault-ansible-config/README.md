# HashiCorp Vault Ansible Config

## Instalace pužívaných Ansible Galaxy kolecí

Na počítači správce, na kterém se spouští Ansible playbook (tedy NE na cílovém severu, který je pomocí Ansible konfigurovaný) spustit:
```bash=
$ ansible-galaxy collection install community.general ansible.posix
```

## Instalace/konfigurace serveru přes Ansible

### Spuštění vybraných úloh z Ansible Playbook

Např. provedení jen úloh s tagy „hostname“ a „timezone“ spustit:
```bash=
ansible-playbook -t hostname -t timezone -i hosts config-server.yml
```

### Kompletní instalace/konfigurace serveru

Spustit skript `config-server-openstack.sh` nebo `config-server-vmware.sh`.

## Ansible Vault – šifrování citlivých dat v konfiguraci Ansible

Ansible Vault slouží pro šifrování celých souborů nebo hodnot proměnných v
konfiguraci Ansible Playbook, např. pro šifrování hesel, která mají být napsána
do konfiguračních souborů na spravovaném serveru (typicky admin hesla k MySQL
databázi v `~/.my.cnf` apod.), ale nechceme je ukládat v otevřené podobě 
v Ansible konfiguraci, která je ukládaná do Gitu.

### Zdroje o Ansible Vault

 * https://www.digitalocean.com/community/tutorials/how-to-use-vault-to-protect-sensitive-ansible-data-on-ubuntu-16-04

 * https://docs.ansible.com/ansible/latest/user_guide/vault.html

### Šifrovací heslo Ansible Vault

Data jsou šiforována heslem. Heslo je **v Gitu uloženo GPG šifrované** 
v souboru `ansible-vault-passwords.asc`.

Aby nebylo nutné heslo zadávat při každém použití Ansible, je možné heslo
**lokálně uložit nešifrovaně** v souboru `ansible-vault-passwords`. Je však
nezbytně nutné **zabrání uložení `ansible-vault-passwords` do Gitu**, proto je
nutné **mít soubor .gitignore**, kde je tento soubor nastaven k ignoraci.

Aby Ansible nástroje věděly, že mají automaticky používat
`ansible-vault-passwords`, existuje soubor `ansible.cfg`:
```bash=
$ cat ansible.cfg
[defaults]
vault_password_file = ./ansible-vault-passwords
```

Tento soubor s heslem pak automaticky používám `ansible-playbook`, `ansible-vault` apod.

### Šifrování souboru pomocí Ansible Vault

Celý existující soubor zašifrujeme:

```bash=
$ ansible-vault encrypt templates/bacula-common_default_passwords.j2
Encryption successful
```

Zašifrovaný soubor prohlédneme:
```bash=
$ ansible-vault view templates/bacula-common_default_passwords.j2
```

Zašifrovaný soubor interaktivně editujeme:
```bash=
$ ansible-vault edit templates/bacula-common_default_passwords.j2
```

Použitý editor se určuje dle nastavení proměnné prostředí `$EDITOR`.

Dešifrování celého souboru:
```bash=
$ ansible-vault decrypt templates/bacula-common_default_passwords.j2
Decryption successful
```

Založení a interaktivní editrace nového souboru:
```bash=
$ ansible-vault create vars/secrets.yml
```

### Šifrování promměné pomocí Ansible Vault

Zašifrování interaktivního vstupu:
```bash=
$ ansible-vault encrypt_string --stdin-name moje_promenna
Reading plaintext input from stdin. (ctrl-d to end input)
Tajná hodnota.
moje_promenna: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          35656336326662656465663239316637326437386466653763663935323737336234313464663164
          3066613732346534313338323336323966323036656538630a343664343036363138356465303733
          38316637373936653563373130386663653663373433643166646665643237366634623537376432
          3662346130643261380a366136303865636630333161376362313066386334663239663463653939
          36656239323331656530376165633134303938373631373062623230646535616239
Encryption successful
```
**Pozor na znak konce nového řádku!** Pokud má být zašifrována jen „Tajná
hodnota.“, nikoliv „Tajná hodnota.\n“, je třeba zmáčknout `Ctrl+D` hned na
konci řetězce, bez vložení `Enter`!

Zašifrování hodnot na příkazové řádce:
```bash=
$ ansible-vault encrypt_string -n moje_promenna 'Tajná hodnota.'
moje_promenna: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          64653239643630666630333431343030386137373561363735616333386538303465633266663536
          3062636131656531316664613033356337376535313665640a323238353466353438356437663136
          39326461613134393661646663326661376332393164326634343834383134663165316365613038
          6238636233343264320a373032383639373036323462376237316537646536363332346137366230
          6133
Encryption successful
```

Výsledek už se dá pak normálně vložit např. do `.yml` konfiguračního souboru a
na proměnnou se odkazovat. Dešifrování probíhá až při použití identicky jako
dešifrování Ansible Vault šifrovaných souborů.

Ověření hodnoty šifrované proměnné:
```bash=
$ ansible localhost --extra-vars @vars/config-server.yml -m ansible.builtin.debug -a var="moje_promenna"
```
