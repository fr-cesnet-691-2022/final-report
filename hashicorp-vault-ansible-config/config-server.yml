---
# This playbook configures the vaultservers nodes

- hosts: vaultservers
  become: yes

  vars_files:
  - vars/config-server.yml
  - vars/lets_encrypt.yml
  - vars/restic-repository-password.yml

  tasks:

  - name: Set hostname
    hostname:
      name: "{{ inventory_hostname }}"
    tags:
      - hostname

  - name: Set timezone
    shell:
      cmd: timedatectl set-timezone "{{ timezone }}"
    tags:
      - timezone

  - name: Use persistent storage for systemd-journald logs
    lineinfile:
      path: /etc/systemd/journald.conf
      regexp: '^[\s#]*Storage=.*$'
      line: 'Storage=persistent'
      state: present
    notify: Restart systemd-journald
    tags:
      - systemd-journald

  - name: Configure SSH deamon
    block:

    - name: SSH server configuration
      lineinfile:
        path: /etc/ssh/sshd_config
        regexp: '^(\s*#*\s*)*({{ item.key }}\s).*$'
        line: '{{ item.key }} {{ item.value }}'
        insertafter: '^# default value.$'
        state: present
      with_dict: "{{ sshd_options }}"
      notify: Reload SSH

    - name: Do not allow password login via SSH for default users
      blockinfile:
        path: /etc/ssh/sshd_config
        state: present
        block: |
          Match User {{ item }}
                 PasswordAuthentication no
        marker: "# {mark} ANSIBLE MANAGED BLOCK: Settings specific for {{ item }} user"
      loop: "{{ default_users }}"
      notify: Reload SSH

    tags:
      - sshd
  
  - name: Configure server firewall
    block:

    - name: Remove old custom firewall files
      file:
        path: "{{ item }}"
        state: absent
      loop: "{{ firewall.old_custom_firewall_files }}"

    - name: Have '{{ root_bin_dir.path }}'
      file:
        path: '{{ root_bin_dir.path }}'
        state: directory
        owner: '{{ root_bin_dir.owner }}'
        group: '{{ root_bin_dir.group }}'
        mode: '{{ root_bin_dir.mode }}'

    - name: Update firewall stop script
      template:
        dest:  "{{ root_bin_dir.path }}/firewall_stop"
        src:   "firewall_stop.j2"
        owner: "root"
        group: "root"
        mode: "u=rwx,go="
      notify: Restart firewall

    - name: Update firewall configuration script
      template:
        dest:  "{{ root_bin_dir.path }}/firewall"
        src:   "firewall.j2"
        owner: "root"
        group: "root"
        mode: "u=rwx,go="
      notify: Restart firewall

    - name: Update firewall SysV init file
      template:
        dest:  "/etc/init.d/firewall"
        src:   "firewall.init.d.j2"
        owner: "root"
        group: "root"
        mode: "u=rwx,go=rx"
      notify: Update SysV init firewall

    - name: Update firewall systemd unit file
      template:
        dest:  "/etc/systemd/system/firewall.service"
        src:   "firewall.service.j2"
        owner: "root"
        group: "root"
        mode: "u=rw,go=r"
      notify: Restart firewall

    tags:
      - firewall

  - name: Install ‘aptitude’ for software installations
    apt:
      name: aptitude
      update_cache: yes
      state: latest
      force_apt_get: yes
    tags:
      - software
      - packages

  - name: Install ‘gpg’ for repositories manipulations
    apt:
      name: gpg
      update_cache: yes
      autoremove: yes
      state: latest
    tags:
      - software
      - repositories
      - packages
      - additional_packages
  
  - name: Install ‘python3’ for script execution
    apt:
      name: python3
      update_cache: yes
      state: latest
      force_apt_get: yes

  - name: Install ‘python3-pip’ for installing required packages and dependencies
    apt:
      name: python3-pip
      update_cache: yes
      state: latest
      force_apt_get: yes

  - name: Ensure Filebeat directory exists
    file:
      path: /etc/filebeat
      state: directory
      mode: "0755"
    tags:
      - filebeat

  - name: Copy Filebeat configuration file
    template:
      src: filebeat.yml.j2
      dest: /etc/filebeat/filebeat.yml
      owner: root
      group: root
      mode: "u=rw,go=r"
    tags:
      - filebeat
    notify: Restart Filebeat

  - name: Add systemd override folder
    file:
      path: /etc/systemd/system/vault.service.d/
      state: directory
      mode: "0755"
    tags:
      - filebeat

  - name: Copy systend vault override file
    copy:
      src: files/vault_systemd_override
      dest: /etc/systemd/system/vault.service.d/override.conf
      owner: root
      group: root
      mode: "u=rw,go=r"
    tags:
      - filebeat

  - name: Make sure a service unit is running
    ansible.builtin.systemd:
      daemon_reload: true
    tags:
        - filebeat

  - name: Copy Filebeat server certificate
    copy:
      src: files/http_ca.crt
      dest: /etc/filebeat/http_ca.crt
      owner: root
      group: root
      mode: "u=rw,go=r"
    tags:
      - filebeat
  
  #- name: Install all requirements and run python script
  #  shell: |
  #    pip install -r requirements.txt
  # #     python3 ./setup.py -p {{ perun_token }} -v {{ vault_token }} -n {{ number_of_groups }}
  #    python3 ./setup.py

  - name: Set packages repositories
    block:

    - name: Add custom packages repositories public signature keys as trusted
      apt_key:
        data: "{{ lookup('file', item.public_key) }}"
        state: present
      loop: "{{ package_repositories }}"
      when: item.public_key is defined
      tags:
        - software-signatures
        
    - name: Add custom packages repositories
      apt_repository:
        repo: "{{ item.repo }}"
        state: "{{ item.state | default(omit) }}"
        filename: "{{ item.filename | default(omit) }}"
      loop: "{{ package_repositories }}"

    - name: Remove contents of /etc/apt/sources.list
      copy:
        dest: '/etc/apt/sources.list'
        content: ''
        force: 'yes'

    tags:
      - software
      - repositories

  - name: Set mode of default users' home directories
    file:
      path: "~{{ item }}/"
      state: directory
      owner: "{{ item }}"
      group: "{{ item }}"
      mode: "u=rwx,go="
    loop: "{{ default_users }}"
    tags:
        - users

  - name: Set password of default user '{{ item }}'
    user:
       name: "{{ item }}"
       password: "{{ default_user_password | default('!') }}"
    loop: "{{ default_users }}"
    tags:
        - users
        - default_user_password

  - name: Set SSH authorized keys of default user
    include_tasks: tasks/default_users_ssh_authorized_keys.yml
    loop: "{{ default_users }}"
    loop_control:
      loop_var: user_item
    tags:
      - default_users_ssh_keys
      - users_ssh_keys

  - name: Configure shell start scripts of default users
    block:

    - name: "Have .bashrc with {{ default_users_umask }} umask"
      blockinfile:
        path: "~{{ item }}/.bashrc"
        state: present
        block: |
          umask {{ default_users_umask }}
        marker: "# {mark} ANSIBLE MANAGED BLOCK: umask"
      loop: "{{ default_users }}"

    - name: Have .bashrc with $EDITOR set
      blockinfile:
        path: "~{{ item }}/.bashrc"
        state: present
        block: |
          export EDITOR="vim"
        marker: "# {mark} ANSIBLE MANAGED BLOCK: EDITOR variable"
      loop: "{{ default_users }}"

    - name: Have .bashrc with ‘ls’ aliases
      blockinfile:
        path: "~{{ item }}/.bashrc"
        state: present
        block: |
          export LS_OPTIONS="${LS_OPTIONS} --color=auto --time-style long-iso --group-directories-first"
          alias ls='ls ${LS_OPTIONS}'
          alias ll='ls -l'
          alias la='ls -lA'
          alias l='ls -AlF'
        marker: "# {mark} ANSIBLE MANAGED BLOCK: ls aliases"
      loop: "{{ default_users }}"

    - name: Have .bashrc with ‘journalctl’ aliases
      blockinfile:
        path: "~{{ item }}/.bashrc"
        state: present
        block: |
          alias jur='journalctl -n 50 -b --no-pager -u'
          alias jurh='journalctl -n 100 -b --no-pager -u'
        marker: "# {mark} ANSIBLE MANAGED BLOCK: journalctl aliases"
      loop: "{{ default_users }}"
  
    - name: Have .bashrc with ‘grep’ alias using colors
      blockinfile:
        path: "~{{ item }}/.bashrc"
        state: present
        block: |
          #
          # Grep bude pri vystupu na terminal obarvovat svuj vystup.
          #
          alias grep='grep --color=auto'
        marker: "# {mark} ANSIBLE MANAGED BLOCK: grep alias"
      loop: "{{ default_users }}"

    - name: Have .bashrc with long history
      blockinfile:
        path: "~{{ item }}/.bashrc"
        state: present
        block: |
          HISTSIZE=10000
          HISTFILESIZE=10000
        marker: "# {mark} ANSIBLE MANAGED BLOCK: history size"
      loop: "{{ default_users }}"

    tags:
      - bashrc

  - name: Upgrade currently-installed software
    apt:
      upgrade: yes
      update_cache: yes
      autoremove: yes
      state: latest
    tags:
      - software
      - packages

  - name: Install additional software
    apt:
      name: "{{ additional_packages }}"
      update_cache: yes
      autoremove: yes
      state: latest
    tags:
      - software
      - packages
      - additional_packages

  - name: Remove unwanted software
    apt:
      name: "{{ unwanted_packages }}"
      update_cache: yes
      autoremove: yes
      purge: yes
      state: absent
    tags:
      - software
      - packages
      - unwanted_packages

  - name: Preparing mount points
    include_tasks: tasks/mount.yml
    loop: "{{ mountpoints }}"
    loop_control:
      loop_var: mountpoint_item
    tags:
      - mountpoints
      - filesystem

  - name: Manipulate users accounts
    block:

    - name: Create users' groups
      group:
        name: "{{ item.name }}"
        gid: "{{ item.gid | default(omit) }}"
        state: "{{ item.state }}"
      when: item.state == 'present'
      loop: "{{ users_groups + additional_groups }}"
      tags:
        - groups

    - name: Create/remove users accounts
      user:
         name: "{{ item.name }}"
         uid: "{{ item.uid | default(omit) }}"
         group: "{{ item.name | default(omit) }}"
         state: "{{ item.state }}"
         comment: "{{ ' '.join(item.firstname | default('') | ternary([item.firstname], [])
                             + item.lastname | default('') | ternary([item.lastname], [])
                             + item.uco | default('') | ternary([item.uco], [])) }}"
         create_home: true
         home: '{{ users_home_root_dir + "/" + item.name }}'
         shell: /bin/bash
         password: "{{ item.password | default('!') }}"
      loop: "{{ users_groups }}"

    - name: Remove users' groups
      group:
        name: "{{ item.name }}"
        gid: "{{ item.gid | default(omit) }}"
        state: "{{ item.state }}"
      when: item.state == 'absent'
      loop: "{{ users_groups + additional_groups }}"
      tags:
        - groups

    - name: Set mode of users' home directories
      file:
        path: '{{ users_home_root_dir + "/" + item.name }}'
        state: directory
        owner: "{{ item.name }}"
        group: "{{ item.name }}"
        mode: "u=rwx,g=rx,o="
      when: item.state == 'present'
      loop: "{{ users_groups }}"

    - name: Set members of additional users' groups
      lineinfile:
        path: /etc/group
        regexp: '^({{ item.name }}:.*?:.*?:).*$'
        line: '\1{{ item.members|sort|join(",") }}'
        backrefs: yes
        state: present
      when: item.state == 'present'
      loop: "{{ additional_groups }}"
      tags:
        - groups

    - name: Have users accounts with SSH authorized keys
      include_tasks: tasks/users_ssh_authorized_keys.yml
      when: user_item.state == 'present'
      loop: "{{ users_groups }}"
      loop_control:
        loop_var: user_item
      tags:
        - users_ssh_keys

    - name: Allow ‘sudo’ users' group members to run commands via sudo without entering passwords
      template:
        dest:  "/etc/sudoers.d/sudo-group-no-password"
        src:   "sudo-group-no-password.j2"
        owner: "root"
        group: "root"
        mode: "ug=r,o="
      tags:
        - sudo

    tags:
      - users

  - name: Configure Postfix Mail Transfer Agent (MTA)
    lineinfile:
      path: /etc/postfix/main.cf
      regexp: '^(\s*#*\s*)*({{ item.key }}\s).*$'
      line: '{{ item.key }} = {{ item.value }}'
      state: present
    with_dict: "{{ postfix_options }}"
    notify: Restart Postfix
    tags:
      - postfix
      - mail

  - name: Configure /etc/aliases to forward root's mails to {{ root_mails_external_email }}
    block:

#    # To keep existing recipients and only add {{ root_mails_external_email }}
#    - name: Check /etc/aliases forwards root's mails to {{ root_mails_external_email }}
#      lineinfile:
#        path: /etc/aliases
#        regexp: '^(root:\s+.*)root, {{ root_mails_external_email }}(.*)$'
#        state: absent
#      check_mode: yes
#      changed_when: false
#      register: status_root_ext_mail
#
#    - name: Set /etc/aliases to forward root's mails to {{ root_mails_external_email }}
#      lineinfile:
#        path: /etc/aliases
#        regexp: '^(root:\s+.*)$'
#        line: '\1, root, {{ root_mails_external_email }}'
#        backrefs: yes
#        state: present
#      notify: Rehash aliases.db
#      when: not status_root_ext_mail.found

     # To replace existing recipients with {{ root_mails_external_email }}
     - name: Set /etc/aliases to forward root's mails to {{ root_mails_external_email }}
       lineinfile:
         path: /etc/aliases
         regexp: '^(root:\s+.*)$'
         line: 'root: root, {{ root_mails_external_email }}'
         state: present
       notify: Rehash aliases.db

    tags:
      - mail

  - name: Configure procmail's forwarding of root's to {{ root_mails_external_email }}
    template:
      dest:  "/root/.forward"
      src:   "procmail-forward.j2"
      owner: "root"
      group: "root"
      mode: "u=rw,go="
    tags:
      - procmail
      - mail

  # Check with: timedatectl show-timesync --all
  - name: Configure NTP servers
    lineinfile:
      path: /etc/systemd/timesyncd.conf
      regexp: '^(\s*#*\s*)*(NTP\s*=\s*).*$'
      line: 'NTP={{ ntp_servers | join(" ") }}'
      insertafter: '[Time]'
      state: present
    notify: Restart systemd-timesyncd
    tags:
      - time

  - name: Configure logwatch
    template:
      dest:  "/etc/logwatch/conf/logwatch.conf"
      src:   "logwatch.conf.j2"
      owner: "root"
      group: "root"
      mode: "u=rw,go=r"
    tags:
      - logwatch

  - name: Configure fail2ban
    template:
      dest:  "/etc/fail2ban/jail.local"
      src:   "jail.local.j2"
      owner: "root"
      group: "root"
      mode: "u=rw,go=r"
    notify: Restart fail2ban
    tags:
      - fail2ban

  #- name: Configure cron-apt
  #  lineinfile:
  #    path: /etc/cron-apt/config
  #    regexp: '^(\s*#*\s*)*({{ item.key }}\s*=).*$'
  #    line: '{{ item.key }}={{ item.value }}'
  #    state: present
  #  with_dict: "{{ cron_apt_options }}"
  #  tags:
  #    - cron_apt

  - name: Configure tmux
    template:
      dest:  "~{{ item }}/.tmux.conf"
      src:   "tmux.conf.j2"
      owner: "{{ item }}"
      group: "{{ item }}"
      mode: "u=rw,go=r"
    loop: "{{ default_users }}"
    tags:
      - tmux

  - name: Configure Vault
    include_tasks: tasks/vault_setup.yml
    tags:
      - vault
      - letsencrypt

  - name: Configure Let's Encrypt
    include_tasks: tasks/lets_encrypt.yml
    tags:
      - letsencrypt

  - name: Configure Let's Encrypt cron job for certificate renewal
    include_tasks: tasks/lets_encrypt_cron_job.yml
    tags:
      - letsencrypt

  - name: Restart vault and configure it to start after system reboot
    ansible.builtin.service:
      name: vault
      state: restarted
      enabled: yes
    become: yes
    tags: vault

  - name: Restart filebeat and configure it to start after system reboot
    ansible.builtin.service:
      name: filebeat
      state: restarted
      enabled: yes
    become: yes

  - name: Configure HAProxy
    include_tasks: tasks/haproxy_setup.yml
    tags:
      - haproxy

  - name: Vault backup cronjob
    ansible.builtin.cron:
      name: Vault backup
      state: present
      minute: "0"
      hour: "*"
      day: "*"
      month: "*"
      weekday: "*"
      job: "vault operator raft snapshot save backup.snap.tar.gz > /var/log/cronlogs 2>&1 "
    tags:
      - cron

  - name: Restic backup cronjob
    ansible.builtin.cron:
      name: Restic backup
      state: present
      minute: "5"
      hour: "*"
      day: "*"
      month: "*"
      weekday: "*"
      job: "restic -r s3:s3.cl2.du.cesnet.cz/vault backup backup.snap.tar.gz; restic -r s3:s3.cl2.du.cesnet.cz/vault forget --keep-daily 30 --keep-weekly 12 --keep-monthly 36 --keep-yearly 75 > /var/log/cronlogs 2>&1"
    tags:
      - restic
      - cron

  - name: Restic backup prune cronjob to delete old backups
    ansible.builtin.cron:
      name: Restic backup
      state: present
      minute: "30"
      hour: "*"
      day: "*"
      month: "*"
      weekday: "1"
      job: "restic -r s3:s3.cl2.du.cesnet.cz/vault forget --keep-daily 30 --keep-weekly 12 --keep-monthly 36 --keep-yearly 75 > /var/log/cronlogs 2>&1"
    tags:
      - restic
      - cron

  - name: Setting environment variable AWS_ACCESS_KEY_ID for restic cron
    community.general.cronvar:
      name: AWS_ACCESS_KEY_ID
      value: "{{restic_repository.access_key}}"
    tags:
      - restic
      - cron

  - name: Setting environment variable AWS_SECRET_ACCESS_KEY for restic cron
    community.general.cronvar:
      name: AWS_SECRET_ACCESS_KEY
      value:  "{{restic_repository.secret_key}}"
    tags:
      - restic
      - cron

  - name: Setting environment variable RESTIC_PASSWORD for restic cron
    community.general.cronvar:
      name: RESTIC_PASSWORD
      value:  "{{restic_repository.password}}"
    tags:
      - restic
      - cron


  - name: Setting environment variable VAULT_ADDR for backup cron
    community.general.cronvar:
      name: VAULT_ADDR
      value: https://{{inventory_hostname}}:8200
    tags:
      - cron 

  - name: Configure Vault
    include_tasks: tasks/haproxy_setup.yml
    tags:
      - haproxy

  handlers:

    - name: Restart firewall
      systemd:
        name: firewall
        state: restarted
        enabled: yes
        force: yes
        daemon_reload: yes

    - name: Update SysV init firewall
      sysvinit:
        name: firewall
        state: restarted
        enabled: yes

    - name: Reload SSH
      service:
        name: ssh
        state: reloaded

    - name: Restart Postfix
      service:
        name: postfix
        state: restarted
    
    - name: Restart Filebeat
      service:
        name: filebeat
        state: restarted
        enabled: yes

    - name: Restart systemd-journald
      service:
        name: systemd-journald
        state: restarted

    - name: Restart systemd-timesyncd
      service:
        name: systemd-timesyncd
        state: restarted

    - name: Restart fail2ban
      service:
        name: fail2ban
        state: restarted

    - name: Rehash aliases.db
      command:
        argv:
          - /usr/bin/newaliases


- hosts: vaultservers-vmware-cesnet
  become: yes
  tasks:
  - name: Set
    include_tasks: tasks/cesnet_vmware_default_ipv4.yml
    tags:
      - cesnetipv4

  handlers:

  - name: Restart networking
    systemd:
      name: networking
      state: restarted
      enabled: yes
      force: yes
      daemon_reload: yes

# vim:set fileencoding=utf-8 expandtab tabstop=2 shiftwidth=2 cursorcolumn:
