from enum import Enum

class Backends(Enum):
    JWT = "jwt"
    DATABASE = "database"
    SSH = "ssh" 
    KUBERNETES = "kubernetes"
    APPROLE = "approle"

    def __str__(self):
        return str(self.value)