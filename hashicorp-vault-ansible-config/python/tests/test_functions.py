import create_perun_and_vault_groups as setup
import backend_manager
import pytest
from backends import Backends
#Testing methods in create-perun-and-vault-groups.py


@pytest.fixture
def args():
    args.perun_token = None
    args.vault_root_token = None
    args.number_of_groups = None
    args.add_managers = None
    return args

def test_generate_policies_for_vault(args):
    #create-perun-and-vault-groups, preparation of the test
    settings = setup.Settings(args)
    vault_manager = setup.VaultManager(settings)
    group_ids = ["1","2","3"]
    group_names = ["desktop","phone","notebook"]
    policy = """
            //to list all folders under kv/

            path "kv/metadata/" {
                capabilities = ["read", "list"]
            }

            //to list all teams under kv/teams/

            path "kv/metadata/teams/" {
                capabilities = [ "read", "list"]
            }

            
            """
    policy_expected = """
        //to list all folders under kv/

        path "kv/metadata/" {
            capabilities = ["read", "list"]
        }

        //to list all teams under kv/teams/

        path "kv/metadata/teams/" {
            capabilities = [ "read", "list"]
        }

        path "{{identity.groups.ids.1.name}}/*" {
        capabilities = ["create", "read", "update", "delete", "list"]
        }
        
        path "{{identity.groups.ids.1.name}}/jwt_*" {
        capabilities = ["create", "read", "update", "delete", "list"]
        }
        
        path "{{identity.groups.ids.1.name}}/kubernetes_*" {
        capabilities = ["create", "read", "update", "delete", "list"]
        }
        
        path "{{identity.groups.ids.1.name}}/approle_*" {
        capabilities = ["create", "read", "update", "delete", "list"]
        }
        
        path "{{identity.groups.ids.1.name}}/delete/*" {
        capabilities = ["update"]
        }

        path "{{identity.groups.ids.1.name}}/undelete/*" {
          capabilities = ["update"]
        }

        path "{{identity.groups.ids.1.name}}/destroy/*" {
          capabilities = ["update"]
        }
        
        path "{{identity.groups.ids.2.name}}/*" {
        capabilities = ["create", "read", "update", "delete", "list"]
        }

        path "{{identity.groups.ids.2.name}}/jwt_*" {
        capabilities = ["create", "read", "update", "delete", "list"]
        }

        path "{{identity.groups.ids.2.name}}/kubernetes_*" {
        capabilities = ["create", "read", "update", "delete", "list"]
        }
        
        path "{{identity.groups.ids.2.name}}/approle_*" {
        capabilities = ["create", "read", "update", "delete", "list"]
        }

        path "{{identity.groups.ids.2.name}}/delete/*" {
        capabilities = ["update"]
        }

        path "{{identity.groups.ids.2.name}}/undelete/*" {
          capabilities = ["update"]
        }

        path "{{identity.groups.ids.2.name}}/destroy/*" {
          capabilities = ["update"]
        }
        
        path "{{identity.groups.ids.3.name}}/*" {
        capabilities = ["create", "read", "update", "delete", "list"]
        }

        path "{{identity.groups.ids.3.name}}/jwt_*" {
        capabilities = ["create", "read", "update", "delete", "list"]
        }
        
        path "{{identity.groups.ids.3.name}}/kubernetes_*" {
        capabilities = ["create", "read", "update", "delete", "list"]
        }

        path "{{identity.groups.ids.3.name}}/approle_*" {
        capabilities = ["create", "read", "update", "delete", "list"]
        }

        path "{{identity.groups.ids.3.name}}/delete/*" {
        capabilities = ["update"]
        }

        path "{{identity.groups.ids.3.name}}/undelete/*" {
          capabilities = ["update"]
        }

        path "{{identity.groups.ids.3.name}}/destroy/*" {
          capabilities = ["update"]
        }
    """
    policy_names_for_deleting_expected = []

    #Act, testing method generate_policies_for_vault
    policy_actual, policy_names_for_deleting_actual = vault_manager.generate_policies_for_vault(policy,group_ids,group_names)
    assert policy_actual.replace("\n","").replace(" ","") == policy_expected.replace("\n","").replace(" ","")
    assert policy_names_for_deleting_actual == policy_names_for_deleting_expected

def test_policies_strings_for_gitlab_apps_access(args):
    #Setup, preparation of the test
    settings = setup.Settings(args)
    vault_manager = setup.VaultManager(settings)
    number_per_group = 1
    group_name = "desktop"
    policy_names_expected = ["AppAccess_jwt_desktop_0"]
    grant_access_expected = ["""
        path "desktop_jwt_0*" {capabilities = ["read"]}
        """]
    #Act, testing method policies_strings_for_gitlab_apps_access
    grant_access_actual, policy_names_actual = vault_manager.policies_strings_for_apps_access(group_name, number_per_group, "jwt")
    assert policy_names_actual.pop() == policy_names_expected.pop()
    assert grant_access_actual[0].replace("\n","").replace(" ","") == grant_access_expected[0].replace("\n","").replace(" ","")

def test_add_backend_policies(args):
    #Setup, preparation of the test
    settings = setup.Settings(args)
    vault_manager = setup.VaultManager(settings)
    group_name = "desktop"
    backend = backend_manager.VaultBackendSettings("mount", "non-jwt", 1)
    policy_names_expected = []
    #Act, testing method add_backend_policies
    policy_names_actual = vault_manager.add_backend_policies(backend, group_name)
    assert len(policy_names_actual) == len(policy_names_expected) == 0

def test_add_kv_policy(args):
    #Setup, preparation of the test
    settings = setup.Settings(args)
    vault_manager = setup.VaultManager(settings)
    group_id = "2"
    policy_added_expected = """
        path "{{identity.groups.ids.2.name}}/*" {
        capabilities = ["create", "read", "update", "delete", "list"]
        }

        path "{{identity.groups.ids.2.name}}/jwt_*" {
        capabilities = ["create", "read", "update", "delete", "list"]
        }
        
        path "{{identity.groups.ids.2.name}}/kubernetes_*" {
        capabilities = ["create", "read", "update", "delete", "list"]
        }
        
        path "{{identity.groups.ids.2.name}}/approle_*" {
        capabilities = ["create", "read", "update", "delete", "list"]
        }
        
        path "{{identity.groups.ids.2.name}}/delete/*" {
        capabilities = ["update"]
        }

        path "{{identity.groups.ids.2.name}}/undelete/*" {
          capabilities = ["update"]
        }

        path "{{identity.groups.ids.2.name}}/destroy/*" {
          capabilities = ["update"]
        }
    """
    #Act, testing method add_kv_policy
    policy_added_actual = vault_manager.add_kv_policy(group_id)
    assert policy_added_expected.replace("\n","").replace(" ","") == policy_added_actual.replace("\n","").replace(" ","")

#Testing methods in backend_manager.py  

def test_create_policies_for_JWT_backend():
    #Setup, preparation of the test
    group_id = "1"
    backend = backend_manager.VaultBackendSettings("mount", Backends.JWT, 1)
    policies_expected = """
        path "mount/{{identity.groups.ids.1.name}}_jwt_0/role/jwt_APP" {
                            capabilities = ["create", "read", "update", "delete", "list"]
                              denied_parameters = {
                                    "policies" = []
                                    }
                        }
        path "auth/{{identity.groups.ids.1.name}}_jwt_0/config" {
                    capabilities = ["create", "read", "update", "delete", "list"]
                }
    """
    #Act, testing method create_policies_for_JWT_backend
    policies_actual = backend_manager.BackendManager.create_policies_for_users_to_manage_backends(backend, group_id)
    assert policies_expected.replace("\n","").replace(" ","") == policies_actual.replace("\n","").replace(" ","")

def test_create_policies_for_db_and_ssh():
    #Setup, preparation of the test
    group_id = "1"
    backend = backend_manager.VaultBackendSettings("mount", Backends.DATABASE, 1)
    policies_expected = """
        path "{{identity.groups.ids.1.name}}_database_*" {
                                   capabilities = ["create", "read", "update", "delete", "list"]  
                               }

        path "mount/{{identity.groups.ids.1.name}}_database_*" {
                                   capabilities = ["create", "read", "update", "delete", "list"]  

                               }
    """

    #Act, testing method create_policies_for_db_and_ssh
    policies_actual = backend_manager.BackendManager.create_policies_for_db_and_ssh(backend, group_id)
    assert policies_expected.replace("\n","").replace(" ","") == policies_actual.replace("\n","").replace(" ","")

if __name__ == '__main__':
    test_policies_strings_for_gitlab_apps_access()