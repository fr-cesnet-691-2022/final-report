from typing import List
from backend_manager import VaultBackendSettings
from oidc import DeviceCodeOAuth
import os

class Settings:
    def __init__(self, args):
        self.perun_token = args.perun_token if args.perun_token is not None else ""
        self.vault_token = args.vault_root_token if args.vault_root_token is not None else ""
        self.add_managers = args.add_managers if args.add_managers is not None else ""
        self.number_of_groups = int(
            args.number_of_groups) if args.number_of_groups is not None else 10
        self.vault_address = os.getenv("VAULT_ADDRESS")
        self.resource_id_in_perun = 2767
        self.perun_parent_group = 11915
        self.perun_hostname = os.getenv("PERUN_ADDRESS")
        self._vault_backends: List[VaultBackendSettings] = []

    def add_managers_to_group_perun(self):
        try:
            managers = self.add_managers.split(",")
            managers_to_return = []    
            for manager in managers:
                try:
                    managers_to_return.append(int(manager))
                except Exception as e:
                    print("The numbers must be given separated by commas in add_managers %s\n" % e)
            return managers_to_return
        except Exception as e:
            print("Problem with parsing add_managers: %s\n" % e)


    def acquire_perun_token(self):
        device_code = DeviceCodeOAuth(os.getenv("PERUN_PROVIDER_FOR_OIDC"),"", False, 0, False)
        token = device_code.get_access_token()
        self.perun_token = token

    def acquire_vault_root_token(self):
        root_token = input("Please enter a root token: ")
        self.vault_token = root_token 

    def add_vault_backends(self, backends: List[VaultBackendSettings]):
        for backend in backends:
            self._vault_backends.append(backend)

    @property
    def vault_backends(self) -> List[VaultBackendSettings]:
        return self._vault_backends