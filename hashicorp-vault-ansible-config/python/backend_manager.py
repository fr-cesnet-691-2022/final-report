import hvac
from backends import Backends

class VaultBackendSettings:
    # TODO: Maybe delete backed_suffix
    def __init__(self, mount: str, type: Backends, number_per_group: int, allowed_parameters= ""):
        """Class to represent paths in policices for different backends"""
        self._mount: str = mount
        self._type: Backends = type
        self._number_per_group: int = number_per_group
        self._allowed_parameters: str = allowed_parameters

    @property
    def mount(self) -> str:
        return self._mount

    @property
    def type(self) -> Backends:
        return self._type

    @property
    def number_per_group(self) -> int:
        return self._number_per_group

    @property
    def allowed_parameters(self) -> str:
        return self._allowed_parameters


class BackendManager:

    def create_policies_for_users_to_manage_backends(backend: VaultBackendSettings, group_id: str) -> str:
        if backend.type == Backends.JWT:
            return BackendManager.create_policies_for_JWT_backend(backend, group_id)

        if backend.type == Backends.APPROLE:
            return BackendManager.create_policies_for_approle_backend(backend, group_id)

        return BackendManager.create_policies_for_db_and_ssh(backend, group_id)

    def create_policies_for_JWT_backend(backend: VaultBackendSettings, group_id: str) -> str:
        grant_users_role_permissions = [
            f"""\n\npath "{backend.mount}/{{{{identity.groups.ids.{group_id}.name}}}}_{backend.type}_{backend_index}/role/{backend.type}_APP" {{
                            capabilities = ["create", "read", "update", "delete", "list"]
                              denied_parameters = {{
                                    "policies" = []
                                    }}
                        }}""" for backend_index in range(backend.number_per_group)]

        grant_users_config_permission = [
            f"""\n\npath "auth/{{{{identity.groups.ids.{group_id}.name}}}}_{backend.type}_{backend_index}/config" {{
                    capabilities = ["create", "read", "update", "delete", "list"]
                }}""" for backend_index in range(backend.number_per_group)]

        return "".join(grant_users_role_permissions) + "\n" + "".join(
            grant_users_config_permission)

    def create_policies_for_db_and_ssh(backend: VaultBackendSettings, group_id: str) -> str:
        grant_user_manage_backend_permission = f"""\n\npath "{backend.mount}/{{{{identity.groups.ids.{group_id}.name}}}}_{backend.type}_*" {{
                                   capabilities = ["create", "read", "update", "delete", "list"]
                                   {backend.allowed_parameters}
                               }}"""

        grant_user_config_backend_permission = f"""\n\npath "{{{{identity.groups.ids.{group_id}.name}}}}_{backend.type}_*" {{
                                   capabilities = ["create", "read", "update", "delete", "list"]
                               }}"""
        return "".join(grant_user_config_backend_permission) + "\n" + "".join(grant_user_manage_backend_permission)

    def create_policies_for_approle_backend(backend: VaultBackendSettings, group_id: str) -> str:
        grant_users_role_permissions = [
            f"""\n\npath "{backend.mount}/{{{{identity.groups.ids.{group_id}.name}}}}_{backend.type}_{backend_index}/role/{backend.type}_APP" {{
                            capabilities = ["create", "read", "update", "delete", "list"]
                            {backend.allowed_parameters}
                        }}""" for backend_index in range(backend.number_per_group)]

        grant_users_config_permission = [
            f"""\n\npath "auth/{{{{identity.groups.ids.{group_id}.name}}}}_{backend.type}_{backend_index}/config" {{
                    capabilities = ["create", "read", "update", "delete", "list"]
                }}""" for backend_index in range(backend.number_per_group)]

        return "".join(grant_users_role_permissions) + "\n" + "".join(
            grant_users_config_permission)