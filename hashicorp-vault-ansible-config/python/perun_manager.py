from typing import List, Optional
import perun_openapi
import os
from perun_openapi.api import groups_manager_api
from perun_openapi.api import resources_manager_api
from perun_openapi.model.input_delete_groups import InputDeleteGroups

from settings import Settings

class PerunManager:
    """Class for representing Perun connection and to maintain its inner state"""

    def __init__(self, settings : Settings):
        """Currently only creates the configuration object based on the global variable with token"""
        self._configuration: Optional[perun_openapi.Configuration] = perun_openapi.Configuration(
            host=os.getenv("PERUN_HOST_RPC_ADDRESS"),
            access_token=settings.perun_token
        )
        self.settings = settings

    def assign_resource_to_group(self, resource_id: int, group_id: int):
        """This function assign resource_id with group_id"""
        with perun_openapi.ApiClient(self._configuration) as api_client:
            api_instance = resources_manager_api.ResourcesManagerApi(
                api_client)
            try:
                res = api_instance.assign_group_to_resource(group_id, resource_id)
                return res
            except perun_openapi.ApiException as e:
                print(
                    "Exception when calling ResourcessManagerApi->assign_group_to_resource: %s\n" % e)

    def create_or_update_group_perun(self, group_name: str, description: str):
        """Function to create a single group in Perun in the VO of MU"""
        with perun_openapi.ApiClient(self._configuration) as api_client:
            # Create an instance of the API class
            api_instance = groups_manager_api.GroupsManagerApi(api_client)
            groups_in_vo = api_instance.get_all_sub_groups(self.settings.perun_parent_group)
            for group in groups_in_vo:
                if group["name"] == f"Vault:{group_name}":
                    return group["id"]

            # If not, then creates it
            res = api_instance.create_group_with_parent_group_name_description(
                self.settings.perun_parent_group, group_name, description)
            return res

    def add_members_to_group(self, group_id: int, members_id: List[int]):
        # ERROR: Does not function for some reason.
        """Function to add users to a group based on their IDs in Perun. Members already in group are skipped"""

        with perun_openapi.ApiClient(self._configuration) as api_client:
            # Create an instance of the API class
            api_instance = groups_manager_api.GroupsManagerApi(api_client)
            res = api_instance.add_members(group_id, members_id)
            return res

    def create_or_update_groups(self) -> List[int]:
        group_ids: List[int] = []
        for i in range(self.settings.number_of_groups):
            res = self.create_or_update_group_perun(f"Group_{i}",
                                                    f"Vault Perun group automatically generated by script - Group{i}")
            if not isinstance(res, int):
                group_ids.append(res["id"])
                self.assign_resource_to_group(
                    self.settings.resource_id_in_perun, res["id"])
            else:
                group_ids.append(res)
                self.assign_resource_to_group(
                self.settings.resource_id_in_perun, res)
        return group_ids

    def delete_groups_perun(self, group_ids : List[int]):
        # Create an instance of the API class
        with perun_openapi.ApiClient(self._configuration) as api_client:
            # Create an instance of the API class
            api_instance = groups_manager_api.GroupsManagerApi(api_client)
            try:
                groups_to_delete = InputDeleteGroups(group_ids, True)
                res = api_instance.delete_groups(groups_to_delete)
                return res
            except Exception as e:
                print("Exception when calling GroupsManagerApi->delete_groups_perun: %s\n" % e)
