import argparse
import os
from typing import List
import requests
from backends import Backends
from policy import Policy
from settings import Settings
from vault_manager import VaultManager
from perun_manager import PerunManager
from dotenv import load_dotenv
from backend_manager import VaultBackendSettings


def parse_cmd_arguments():
    """This function proccesses the arguments given in command line"""
    parser = argparse.ArgumentParser(
        description='Parse PERUN_TOKEN, VAULT_TOKEN and number of groups')

    parser.add_argument(
        '-p', '--perun_token', required=False, help='Token, which will be used for PERUN authetication. Leave blank if you want to use the web authentication', type=str)
    parser.add_argument(
        '-v', '--vault_root_token', required=False, help='Token, which will be used for VAULT authentication',
        type=str)
    parser.add_argument(
        '-n', '--number_of_groups', required=False, help='Number of groups, which will be created', type=str)
    parser.add_argument(
        '-d', '--disable_perun', required=False, action='store_true', help='This flag will disable fetching data from Perun')
    parser.add_argument(
        '-a', '--add_managers', required=False, help='This flag will add managers to a group', type=str)
    parser.add_argument(
        '-b', '--number_of_backends', required=False, help='Number of backends per group, which will be created', type=str)

    _error = parser.error

    def error(message):
        print('argparse parser error %s', message)
        _error(message)

    parser.error = error

    return parser.parse_args()

def parse_number_of_backends(args):

    #The default number of backends per group is 1
    DEFAULT_NUMBER_OF_BACKENDS = 1
    if args.number_of_backends is None:
        return DEFAULT_NUMBER_OF_BACKENDS

    try:
        return int(args.number_of_backends)
    except Exception as e:
        print("The numbers of backends should be integer, I will use default number %s\n" % e)
    return DEFAULT_NUMBER_OF_BACKENDS


if __name__ == '__main__':
    # Try to create some groups with its policies and aliases
    name_of_policy = "user"
    user_policy = """
            //to list all folders under kv/

            path "kv/metadata/" {
                capabilities = ["read", "list"]
            }

            //to list all teams under kv/teams/

            path "kv/metadata/teams/" {
                capabilities = [ "read", "list"]
            }

            """
    group_name = "Group"

    ssh_allowed_parameters = """
    allowed_parameters = {
                    "type" = ["ssh"]
                    "*" = []
                  }"""

    jwt_allowed_parameters = f"""
    allowed_parameters = {{
                   "jwks_url" = ["{os.getenv('JWKS_URL_ALLOWED_PARAMETER')}"]
                   "*" = []
                    }}"""



    load_dotenv()
    policy = Policy(name_of_policy, user_policy)
    args = parse_cmd_arguments()
    settings = Settings(args)

    number_of_backends = parse_number_of_backends(args)

    vault_backends: List[VaultBackendSettings] = [
        VaultBackendSettings("auth", Backends.JWT, number_of_backends), VaultBackendSettings("sys/mounts", Backends.DATABASE, number_of_backends),
        VaultBackendSettings("sys/mounts", Backends.SSH, number_of_backends, ssh_allowed_parameters), VaultBackendSettings("auth", Backends.KUBERNETES, number_of_backends,),
        VaultBackendSettings("auth", Backends.APPROLE, number_of_backends)]


    settings.add_vault_backends(vault_backends)

    settings.acquire_vault_root_token()

    if not args.disable_perun:
        settings.acquire_perun_token()
    
    if args.add_managers is not None:
        perun_managers = settings.add_managers_to_group_perun()
    else:
        perun_managers = []

    # First of all, we are establishing connection with Vault
    try:
        vault_manager = VaultManager(settings)
    except Exception as e:
        print(f"Problem with establishing connection with Vault - {str(e)}")

    if not args.disable_perun:
        #Then, we are establishing connection with Perun
        try:
            perun_manager = PerunManager(settings)
        except Exception as e:
            print(f"Problem with establishing connection with Perun - {str(e)}")

    # Create groups, their aliases and policies in Vault
    try:
        group_ids, alias_ids, backend_paths, policy_names = \
            vault_manager.create_or_update_groups(group_name, policy)
    except Exception as e:
        print(f"Problem with creating groups in Vault - {str(e)}")

    if not args.disable_perun:
        try:
            group_ids = perun_manager.create_or_update_groups()
            member_ids = [167091]
            if perun_managers != []:
             member_ids.extend(perun_managers)
            for group_id in group_ids:
                perun_manager.add_members_to_group(group_id, member_ids)
        except Exception as e:
            print(f"Problem with creating groups in Perun - {str(e)}")





