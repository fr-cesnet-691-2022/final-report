from itertools import permutations
import hvac
import os
from backends import Backends
from backend_manager import BackendManager, VaultBackendSettings
from typing import List, Tuple
from policy import Policy


class VaultManager:
    def __init__(self, settings):
        self.settings = settings
        self.client = hvac.Client(
            url=self.settings.vault_address, token=self.settings.vault_token)

    def read_group(self, group_name):
        """This function returns group_id based on group_name"""
        read_response = self.client.secrets.identity.read_group_by_name(
            name=group_name)
        return read_response['data']['id']

    def create_or_update_group(self, group_name, extra_information="", policies=None):
        """This function creates one group based on its name and extra_information and returns its id, if a group is already created, it returns its id"""
        create_response = self.client.secrets.identity.create_or_update_group(
            name=group_name,
            metadata=dict(extra_datas=extra_information),
            group_type="external",
            policies=policies)
        try:
            return create_response['data']['id']
        except Exception as e:
            print(f"Group with name {group_name} already exists, skipping")
            return self.read_group(group_name)

    def create_policy(self, policy: Policy):
        """This function creates a policy based on its name and specification"""
        try:
            self.client.sys.create_or_update_policy(
                name=policy.name_of_policy,
                policy=policy.policy)
        except Exception as e:
            print(f"Policy {policy.policy} is already present - {str(e)}")
            return

    def create_role(self, mount_path: str, policy_name_in_vault: str, role_name: str, backend_type: Backends):
        """MVP implementation for adding roles with preconfigured policy to auth backends"""
        if backend_type == Backends.JWT:
            self.client.auth.jwt.create_role(name=role_name, role_type=str(backend_type), token_policies=[policy_name_in_vault],
                                             path=mount_path, user_claim="user_email", bound_claims={
                    "project_id": "22",
                    "ref": "master",
                    "ref_type": "branch"
                }, allowed_redirect_uris=None)
        if backend_type == Backends.KUBERNETES:
            self.client.auth.kubernetes.create_role(name=role_name, bound_service_account_names="*", bound_service_account_namespaces="*", policies=[policy_name_in_vault],
                                             mount_point=mount_path)

    def delete_policy(self, policy_name: str):
        self.client.sys.delete_policy(name=policy_name)

    def print_all_groups(self):
        """This is an auxiliary function for printing all groups"""
        list_response = self.client.secrets.identity.list_groups_by_name()
        group_keys = list_response['data']['keys']
        print(group_keys)

    def get_oidc_id(self) -> str:
        #Assumes, that there is only one OIDC path to get
        return  self.client.sys.list_auth_methods()["data"][os.getenv("OIDC_PATH_IN_VAULT")]["accessor"]

    def create_alias_for_group(self, group_id, group_name):
        """This function creates alias for group in a particular form and returns its alias_id"""
        try:
            ALIAS = f"{os.getenv('CLAIM_PREFIX')}{group_name.lower().capitalize()}{os.getenv('CLAIM_SUFFIX')}"
            create_response = self.client.secrets.identity.create_or_update_group_alias(
                name=ALIAS,
                canonical_id=group_id,
                mount_accessor=self.get_oidc_id(),  # Check that mount accessor
            )
            return create_response['data']['id']
        except Exception as e:
            print(f"Alias for group {group_name} already exists, skipping")
            return

    def delete_alias(self, alias_id):
        self.client.secrets.identity.delete_group_alias(entity_id=alias_id)

    def delete_group(self, group_id):
        self.client.secrets.identity.delete_group(group_id=group_id)

    def create_groups_and_aliases_in_vault(self, group_names: List[str]) -> Tuple[List[str], List[str]]:
        """This function creates groups with extra information specified and their policies in a list of tuples consisting of (group_name, group_extra_information,policies_associated_with_group)"""
        # TODO: Handle when client is NOT authenticated, e.g. incorrect root token.
        if not self.client.is_authenticated():
            print("Client authentication FAILED")
            raise Exception("Authentication to Vault failed!!!")

        group_ids: List[str] = []
        alias_ids: List[str] = []
        for current_group_name in group_names:
            group_id = self.create_or_update_group(current_group_name)
            group_ids.append(group_id)
            alias_id = self.create_alias_for_group(
                group_id, current_group_name)
            alias_ids.append(alias_id)
        return group_ids, alias_ids

    #TODO: Use policies_names_created
    def create_policies_for_app_access(self, group_name: str, number_per_group: int, backend_type: Backends) -> List[str]:
        """This method creates policies, that grant access to subspaces of kv store of a specific team. This enables. e.g. GitLab or Kubernetes to  read parts of kv store that is
        dedicated to it instead of having access to all of the secrets"""

        grant_app_kv_read_permission, policy_names = self.policies_strings_for_apps_access(group_name, number_per_group, backend_type)
        approle_read_permission, approle_policy_names = [], []
        if backend_type == Backends.DATABASE:
            approle_read_permission, approle_policy_names = self.policies_string_access_to_db(backend_type)

        grant_app_kv_read_permission += approle_read_permission
        policy_names += approle_policy_names

        policies_names_created = []
        for policy,name in zip(grant_app_kv_read_permission, policy_names):
            if self.create_policy(Policy(name, policy)) is not None:
                policies_names_created.append(name)
        return policy_names

    def policies_string_access_to_db(self, backend_type: Backends) -> Tuple[
        List[str], List[str]]:
        all_secret_engines: dict = self.client.sys.list_mounted_secrets_engines()
        # Mounted secret engines contain /sys, datam renewable, lease_duration etc, we only want db backends. Also remove trailing / from engine
        database_secret_engines = [engine[:-1] for engine in
                                   list(filter(lambda backend: "database" in backend, all_secret_engines.keys())) if
                                   all_secret_engines[engine]['type'] == 'database']
        permission = [f""" \n\npath "{database_engine}/cred" {{
            capabilities = ["read"]
            }}  """ for database_engine in database_secret_engines]
        policy_names = [f'Access_{backend_type}_{database_engine.replace("database_","")}' for database_engine in database_secret_engines]
        return permission, policy_names

    def policies_strings_for_apps_access(self, group_name: str, number_per_group: int, backend_type: Backends) -> Tuple[List[str], List[str]]:

        if backend_type == Backends.APPROLE:
            grant_app_kv_read_permission = [
                f"""\n\npath "{group_name}_{backend_type}_{backed_index}*" {{
            capabilities = ["read", "list"]
            }}
            """ for backed_index in range(number_per_group)]
        else:
            grant_app_kv_read_permission = [
                f"""\n\npath "{group_name}_{backend_type}_{backed_index}*" {{
            capabilities = ["read"]
            }}
            """ for backed_index in range(number_per_group)]

        policy_names = [f"AppAccess_{backend_type}_{group_name}_{backend_index}" for backend_index in
                        range(number_per_group)]
        return grant_app_kv_read_permission, policy_names

    # This is a bit ugly. However it is diffucult to find a nice solution. We need to create standalone policies, that are designed
    # by the "subpolicies" in the global user policy. The question is, if its better here like this, or whether we should make an instance
    # of BackendManager, that would create these and then return the ids of the created policies, so we could mantain atomicity.
    # TODO: Why is policy taken as a parameter of this function and never used ? Removed it for testing puposes
    def add_backend_policies(self, backend: VaultBackendSettings, group_name: str) -> \
            List[str]:
        policy_names = []
        if backend.type in [Backends.JWT, Backends.DATABASE, Backends.KUBERNETES, Backends.APPROLE]:
            policy_names = self.create_policies_for_app_access(group_name, backend.number_per_group, backend.type)
            for i, policy_name in enumerate(policy_names):
                self.create_role(f"{group_name}_{backend.type}_{i}", policy_name, f"{backend.type}_APP", backend.type)
        return policy_names

    def add_kv_policy(self, group_id: str) -> str:
        return f"""\n\npath "{{{{identity.groups.ids.{group_id}.name}}}}/*" {{
        capabilities = ["create", "read", "update", "delete", "list"]
        }}

        path "{{{{identity.groups.ids.{group_id}.name}}}}/{Backends.JWT}_*" {{
        capabilities = ["create", "read", "update", "delete", "list"]
        }}

        path "{{{{identity.groups.ids.{group_id}.name}}}}/{Backends.KUBERNETES}_*" {{
        capabilities = ["create", "read", "update", "delete", "list"]
        }}

        path "{{{{identity.groups.ids.{group_id}.name}}}}/{Backends.APPROLE}_*" {{
        capabilities = ["create", "read", "update", "delete", "list"]
        }}

        path "{{{{identity.groups.ids.{group_id}.name}}}}/delete/*" {{
        capabilities = ["update"]
        }}
        path "{{{{identity.groups.ids.{group_id}.name}}}}/undelete/*" {{
          capabilities = ["update"]
        }}
        path "{{{{identity.groups.ids.{group_id}.name}}}}/destroy/*" {{
          capabilities = ["update"]
        }}
        """

    def format_strings(self, strings: List[str]) -> str:
        return "[" + ", ".join('"{}"'.format(s) for s in strings) + "]"

    def create_formated_list_of_strings_from_tuple(self, tuple : Tuple[str]) -> List[str]:
        return list(tuple)

    def format_list_of_lists_string(self, list_of_lists : List[List[str]]) -> str:
        str_to_return = "["
        for lst in list_of_lists:
            str_to_return += "[" + ", ".join('"{}"'.format(s) for s in lst) + "], "
        str_to_return = str_to_return[:-2:]
        str_to_return += "]"
        return str_to_return

    def get_approle_policy_name(self, approle_policy: str) -> str:
        """This function just takes the name of approle policy and adds _APPROLE to it, to create unique name"""
        return f"{approle_policy}_APPROLE"

    def create_approle_policies(self, approle_policy_name : str) -> str:
        name_of_policy = self.get_approle_policy_name(approle_policy_name)
        policy = f"""\n\npath "auth/{approle_policy_name}/role/+/secret*" {{
                        capabilities = [ "create", "read", "update" ]
                        min_wrapping_ttl = "100s"
                        max_wrapping_ttl = "300s"}}
                """
        approle_policy = Policy(name_of_policy,policy)
        self.create_policy(approle_policy)
        return name_of_policy
    
    def get_approle_for_append(self, lst_contaning_approle: List[str]) -> str:
        approle_policy = list(filter(lambda policy : "approle" in policy, lst_contaning_approle))[0]
        #returned policy, which is in the list of approle policies in format {policy}_APPROLE as I previously named it like that
        return self.get_approle_policy_name(approle_policy)

    def generate_policies_for_vault(self, policy: str,
                                    group_ids: List[str], group_names: List[str]) -> Tuple[str, List[str]]:
        policy_names_for_deleting = []
        policies_for_approle = []
        for group_name in group_names:
            for backend in self.settings.vault_backends:
                policy_names = self.add_backend_policies(backend, group_name)
                policy_names_for_deleting = policy_names_for_deleting + policy_names
                #Create policies for APPROLE
                if backend.type == Backends.APPROLE:
                    for policy_name in policy_names:
                        approle_policy = self.create_approle_policies(policy_name)
                        policies_for_approle.append(approle_policy)

        for group_number, group_id in enumerate(group_ids):
            policy += self.add_kv_policy(group_id)
            for backend in self.settings.vault_backends:
                if backend.type == Backends.APPROLE or backend.type == Backends.KUBERNETES:
                    policy_names_for_formating = list(set(filter(lambda policy_name: f"Group_{group_number}" in policy_name,
                                                             policy_names_for_deleting)))
                    
                    allowed_policies_list = list(map(self.create_formated_list_of_strings_from_tuple, permutations(policy_names_for_formating)))
                    
                    if backend.type == Backends.APPROLE:
                        approle_policy = self.get_approle_for_append(allowed_policies_list[0])
                        for lst in allowed_policies_list:
                            lst.append(approle_policy)

                    allowed_policies = self.format_list_of_lists_string(allowed_policies_list)
                    
                    allowed_parameters = f"""
                    allowed_parameters = {{
                                    "token_policies" = {allowed_policies}
                                    "ttl" = []"""
                    
                    if backend.type == Backends.KUBERNETES:
                        allowed_parameters += """
                                    "token_reviewer_jwt" = []
                                    "kubernetes_host" = []
                                    "kubernetes_ca_cert" = []
                                    "bound_service_account_names" = []
                                    "bound_service_account_namespaces" = []"""

                    allowed_parameters += "}"
                
                    backend._allowed_parameters = allowed_parameters

                policy += BackendManager.create_policies_for_users_to_manage_backends(backend, group_id)

        policy_names_for_deleting = policy_names_for_deleting + policies_for_approle
        return policy, policy_names_for_deleting

    def delete_kv_engine(self) -> None:
        ENGINE_TYPE = "kv"
        secrets_engines_list = self.client.sys.list_mounted_secrets_engines()['data'].keys()
        if f"{ENGINE_TYPE}/" not in secrets_engines_list:
            self.client.sys.disable_secrets_engine(path=ENGINE_TYPE)

    def create_kv_engine(self, group_name) -> None:
        ENGINE_TYPE = "kv"
        secrets_engines_list = self.client.sys.list_mounted_secrets_engines()['data'].keys()
        if f"{group_name}/" not in secrets_engines_list:
            self.client.sys.enable_secrets_engine(ENGINE_TYPE, path=f"{group_name}", options=dict({"version": "2"}))

    def create_or_update_groups(self, group_name_base: str, policy: Policy) -> Tuple[List[str],List[str],List[str],List[str]]:
        group_names = [group_name_base + "_" +
                       str(i) for i in range(self.settings.number_of_groups)]
        group_ids, alias_ids = self.create_groups_and_aliases_in_vault(
            group_names)
        # Aktuálně garantované, že budou i pro všechny skupiny group_ids
        backend_paths: List[str] = self.enable_backends(group_names)
        policy.policy, policy_names = self.generate_policies_for_vault(policy.policy, group_ids, group_names)
        self.create_policy(policy)
        for group_name in group_names:
            self.create_kv_engine(group_name)
            self.create_group_section_in_kv(group_name)
        return group_ids, alias_ids, backend_paths, policy_names

    def create_group_section_in_kv(self, group_name: str) -> None:
        mount_point = f"{group_name}"
        self.client.secrets.kv.v2.create_or_update_secret(path=f'{group_name}', secret=dict(pssst='This is a secret'), mount_point=mount_point)
        self.client.secrets.kv.v2.create_or_update_secret(path=f'{group_name}/Subpath', secret=dict(pssst='This is a secret'), mount_point=mount_point)
        self.client.secrets.kv.v2.create_or_update_secret(path=f'{group_name}_jwt_', secret=dict(pssst='This is a secret for JWT'), mount_point=mount_point)
        self.client.secrets.kv.v2.create_or_update_secret(path=f'{group_name}_approle_', secret=dict(pssst='This is a secret for AppRole'), mount_point=mount_point)

    def enable_backends(self, group_names: List[str]) -> List[str]:
        backend_paths: List[str] = []
        for current_group_name in group_names:
            for backend in self.settings.vault_backends:
                for backend_index in range(backend.number_per_group):
                    self.handle_enabling_single_backend(backend, backend_paths, backend_index, current_group_name)
        return backend_paths

    def handle_enabling_single_backend(self, backend, backend_paths, backend_index, current_group_name) -> None:
        joined_backends_list = self.get_list_of_auth_and_secret_backends()
        backed_path: str = f"{current_group_name}_{backend.type}_{backend_index}/"
        if backed_path not in joined_backends_list:
            response = self.enable_backend(backend, backed_path)
            if response.status_code != 204:
                raise Exception("Unexpected return value from Vault when enabling an auth or secret backend")
            backend_paths.append(backed_path)

    def get_list_of_auth_and_secret_backends(self) -> List[str]:
        secrets_engines_list = self.client.sys.list_mounted_secrets_engines()['data'].keys()
        auth_methods_list = self.client.sys.list_auth_methods()["data"].keys()
        joined_backends_list = list(secrets_engines_list) + list(auth_methods_list)
        return joined_backends_list

    def enable_backend(self, backend: VaultBackendSettings, backend_path_string: str):
        """This method enables a given backend based on the BackendSetting. The settings object can be the same for both, but the api call is different and needs to be correctly chosen"""
        if backend.mount == "auth":
            response = self.client.sys.enable_auth_method(str(backend.type),
                                                          path=backend_path_string)
        else:
            response = self.client.sys.enable_secrets_engine(str(backend.type), path=backend_path_string)
        return response
