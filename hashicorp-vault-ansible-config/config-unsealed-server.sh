#!/bin/bash
ansible-playbook -i hosts config-unsealed-server.yml --extra-vars @vars/vault-vars.yml --vault-password-file=ansible-vault-passwords
