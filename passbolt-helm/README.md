# Helm Chart for Passbolt Deployment 

This file is a modified version of the [official](https://github.com/passbolt/charts-passbolt) passbolt helm charts. 

There were some differences. Therefore we needed to use this version on our Kubernetes. This also contains the required setup for the application to be available on passbolt.example.com. This chart also adds backup capability, which uses a custom restic container to push the data to CESNET S3 storage. 

To run the helm chart, CHANGE all the credentials marked with CHANGME and other options to suit your need in both 
* values.yaml
* secrets.yaml

Then run in this directory `helm install deployment name charts-passbolt/ -f values.yaml -f secrets.yaml`

