#!bin/sh

export RESTIC_PASSWORD=$(cat /restic_password/repositoryPassword  )
restic -r s3:s3.cl2.du.cesnet.cz/passbolt backup /backup
restic -r s3:s3.cl2.du.cesnet.cz/passbolt forget --keep-daily 30 --keep-weekly 12 --keep-monthly 36 --keep-yearly 75
