# PassBolt Restic Backup

Repository for our restic container used for Kubernetes cronjob backups.

The container has the address for the S3 storage defined in `start.sh`.
